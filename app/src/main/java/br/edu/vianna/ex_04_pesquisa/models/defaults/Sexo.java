package br.edu.vianna.ex_04_pesquisa.models.defaults;

public enum Sexo {
    MASCULINO,
    FEMININO
}
