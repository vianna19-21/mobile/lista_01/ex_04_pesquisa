package br.edu.vianna.ex_04_pesquisa.models;

import java.io.Serializable;
import java.util.ArrayList;

import br.edu.vianna.ex_04_pesquisa.models.defaults.CorCabelos;
import br.edu.vianna.ex_04_pesquisa.models.defaults.CorOlhos;
import br.edu.vianna.ex_04_pesquisa.models.defaults.Sexo;

public class Pesquisa implements Serializable {
    private ArrayList<Pessoa> pessoas;

    public Pesquisa() {
        this.pessoas = new ArrayList<Pessoa>();
    }

    public void addPessoa(Pessoa p) {
        this.pessoas.add(p);
    }

    public int maiorAltura() {
        int maior = pessoas.get(0).getAltura();

        for (Pessoa p : pessoas) {
            if (p.getAltura() > maior) {
                maior = p.getAltura();
            }
        }

        return maior;
    }

    public int menorAltura() {
        int menor = pessoas.get(0).getAltura();

        for (Pessoa p : pessoas) {
            if (p.getAltura() < menor) {
                menor = p.getAltura();
            }
        }

        return menor;
    }

    public double mediaAlturaPorSexo(Sexo s) {
        double media = 0;

        for (Pessoa p : pessoas) {
            if (p.getSexo() == s) {
                media += p.getAltura();
            }
        }

        return media/contaPorSexo(s);
    }

    public int contaPorSexo(Sexo s) {
        int count = 0;

        for (Pessoa p : pessoas) {
            if (p.getSexo() == s) {
                count++;
            }
        }

        return count;
    }

    public double porcentagemPorSexo(Sexo s) {
        return contaPorSexo(s) * 100 / pessoas.size();
    }

    public double porcentagemQueryCompleta(Sexo s, int idadeMin, int idadeMax, CorCabelos c, CorOlhos o) {
        int count = 0;

        for (Pessoa p : pessoas) {
            if (
                    p.getSexo() == s &&
                    p.getIdade() >=idadeMin && p.getIdade() <= idadeMax &&
                    p.getCabelos() == c &&
                    p.getOlhos() == o
            ) {
                count ++;
            }
        }

        return count * 100 / pessoas.size();
    }
}
