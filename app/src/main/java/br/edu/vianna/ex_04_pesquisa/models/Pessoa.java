package br.edu.vianna.ex_04_pesquisa.models;

import java.io.Serializable;

import br.edu.vianna.ex_04_pesquisa.models.defaults.CorCabelos;
import br.edu.vianna.ex_04_pesquisa.models.defaults.CorOlhos;
import br.edu.vianna.ex_04_pesquisa.models.defaults.Sexo;

public class Pessoa implements Serializable {
    private Sexo sexo;
    private CorOlhos olhos;
    private CorCabelos cabelos;
    private int idade, altura;

    public Pessoa() {
    }

    public Pessoa(Sexo sexo, CorOlhos olhos, CorCabelos cabelos, int idade, int altura) {
        this.sexo = sexo;
        this.olhos = olhos;
        this.cabelos = cabelos;
        this.idade = idade;
        this.altura = altura;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public CorOlhos getOlhos() {
        return olhos;
    }

    public void setOlhos(CorOlhos olhos) {
        this.olhos = olhos;
    }

    public CorCabelos getCabelos() {
        return cabelos;
    }

    public void setCabelos(CorCabelos cabelos) {
        this.cabelos = cabelos;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }
}
