package br.edu.vianna.ex_04_pesquisa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import br.edu.vianna.ex_04_pesquisa.models.database.FakeDatabase;
import br.edu.vianna.ex_04_pesquisa.models.defaults.CorCabelos;
import br.edu.vianna.ex_04_pesquisa.models.defaults.CorOlhos;
import br.edu.vianna.ex_04_pesquisa.models.defaults.Sexo;

public class RelatorioActivity extends AppCompatActivity {
    private TextView txtMaxAltura, txtMinAltura, txtMediaMulheres, txtNumHomens, txtPercentHomem,
        txtPercentMulher, txtQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio);
        setTitle("Relatório");

        bindings();

        gerarRelatorio();
    }

    private void gerarRelatorio() {
        txtMaxAltura.setText(String.valueOf(FakeDatabase.pesquisa.maiorAltura()));
        txtMinAltura.setText(String.valueOf(FakeDatabase.pesquisa.menorAltura()));
        txtMediaMulheres.setText(String.valueOf(FakeDatabase.pesquisa.mediaAlturaPorSexo(Sexo.FEMININO)));
        txtNumHomens.setText(String.valueOf(FakeDatabase.pesquisa.contaPorSexo(Sexo.MASCULINO)));
        txtPercentHomem.setText(String.valueOf(FakeDatabase.pesquisa.porcentagemPorSexo(Sexo.MASCULINO)));
        txtPercentMulher.setText(String.valueOf(FakeDatabase.pesquisa.porcentagemPorSexo(Sexo.FEMININO)));
        txtQuery.setText(String.valueOf(FakeDatabase.pesquisa.porcentagemQueryCompleta(
                Sexo.FEMININO, 18, 35, CorCabelos.LOUROS, CorOlhos.AZUIS)));
    }

    private void bindings() {
        txtMaxAltura = findViewById(R.id.txtMaxAltura);
        txtMinAltura = findViewById(R.id.txtMinAltura);
        txtMediaMulheres = findViewById(R.id.txtMediaMulheres);
        txtNumHomens = findViewById(R.id.txtNumHomens);
        txtPercentHomem = findViewById(R.id.txtPercentHomem);
        txtPercentMulher = findViewById(R.id.txtPercentMulher);
        txtQuery = findViewById(R.id.txtQuery);
    }
}