package br.edu.vianna.ex_04_pesquisa.models.defaults;

public enum CorCabelos {
    LOUROS,
    CASTANHOS,
    PRETOS
}
