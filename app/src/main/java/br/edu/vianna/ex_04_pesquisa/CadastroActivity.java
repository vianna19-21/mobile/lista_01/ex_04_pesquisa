package br.edu.vianna.ex_04_pesquisa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import br.edu.vianna.ex_04_pesquisa.models.Pessoa;
import br.edu.vianna.ex_04_pesquisa.models.database.FakeDatabase;
import br.edu.vianna.ex_04_pesquisa.models.defaults.CorCabelos;
import br.edu.vianna.ex_04_pesquisa.models.defaults.CorOlhos;
import br.edu.vianna.ex_04_pesquisa.models.defaults.Sexo;

public class CadastroActivity extends AppCompatActivity {
    private RadioButton rdMasculino, rdFeminino, rdLouros, rdCastanhos, rdPretos, rdVerdes, rdAzuis,
        rdOlhosCastanhos;
    private TextInputEditText txtAltura, txtIdade;
    private Button btnSalvar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        setTitle("Cadastro");

        bindings();

        btnSalvar.setOnClickListener(callSalvar());
    }

    private View.OnClickListener callSalvar() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CorOlhos olhos = null;
                CorCabelos cabelos = null;
                Sexo sexo = null;
                int idade = Integer.parseInt(txtIdade.getText().toString());
                int altura = Integer.parseInt(txtAltura.getText().toString());

                if (rdAzuis.isChecked()) olhos = CorOlhos.AZUIS;
                if (rdVerdes.isChecked()) olhos = CorOlhos.VERDES;
                if (rdOlhosCastanhos.isChecked()) olhos = CorOlhos.CASTANHOS;

                if (rdMasculino.isChecked()) sexo = Sexo.MASCULINO;
                if (rdFeminino.isChecked()) sexo = Sexo.FEMININO;

                if (rdLouros.isChecked()) cabelos = CorCabelos.LOUROS;
                if (rdCastanhos.isChecked()) cabelos = CorCabelos.CASTANHOS;
                if (rdPretos.isChecked()) cabelos = CorCabelos.PRETOS;

                Pessoa p = new Pessoa(sexo, olhos, cabelos, idade, altura);

                FakeDatabase.save(p);

                Toast.makeText(getApplicationContext(), "Salvo com sucesso!", Toast.LENGTH_LONG).show();

                finish();
            }
        };
    }

    private void bindings() {
        rdMasculino = findViewById(R.id.rdMasculino);
        rdFeminino = findViewById(R.id.rdFeminino);
        rdLouros = findViewById(R.id.rdLouros);
        rdCastanhos = findViewById(R.id.rdCastanhos);
        rdPretos = findViewById(R.id.rdPretos);
        rdVerdes = findViewById(R.id.rdVerdes);
        rdAzuis = findViewById(R.id.rdAzuis);
        rdOlhosCastanhos = findViewById(R.id.rdOlhosCastanhos);

        txtAltura = findViewById(R.id.txtAltura);
        txtIdade = findViewById(R.id.txtIdade);

        btnSalvar = findViewById(R.id.btnSalvar);
    }
}