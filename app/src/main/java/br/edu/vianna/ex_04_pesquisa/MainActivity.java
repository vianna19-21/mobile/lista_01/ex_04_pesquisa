package br.edu.vianna.ex_04_pesquisa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import br.edu.vianna.ex_04_pesquisa.models.Pessoa;
import br.edu.vianna.ex_04_pesquisa.models.database.FakeDatabase;

public class MainActivity extends AppCompatActivity {
    private Button btnCadastro, btnRelatorio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Pesquisa");

        bindings();

        btnCadastro.setOnClickListener(callCadastro());
        btnRelatorio.setOnClickListener(callRelatorio());

    }

    private View.OnClickListener callRelatorio() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(getApplicationContext(), RelatorioActivity.class);

                startActivityForResult(it, 1);
            }
        };
    }

    private View.OnClickListener callCadastro() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(getApplicationContext(), CadastroActivity.class);

                startActivityForResult(it, 1);
            }
        };
    }

    private void bindings() {
        btnCadastro = findViewById(R.id.btnCadastro);
        btnRelatorio = findViewById(R.id.btnRelatorio);
    }
}