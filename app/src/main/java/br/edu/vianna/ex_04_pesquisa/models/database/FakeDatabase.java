package br.edu.vianna.ex_04_pesquisa.models.database;

import br.edu.vianna.ex_04_pesquisa.models.Pesquisa;
import br.edu.vianna.ex_04_pesquisa.models.Pessoa;

public class FakeDatabase {
    public static Pesquisa pesquisa = new Pesquisa();

    public static void save(Pessoa p) {
        pesquisa.addPessoa(p);
    }
}
